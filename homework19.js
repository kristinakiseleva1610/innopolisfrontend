"use strict";

// Упражнение 1
let a = '$100';
let b = '300$';
a = +a.slice(1);
b = +parseInt(b);
let summ = a + b;
console.log(summ);
// Упражнение 2
let message = ' привет, медвед      ';
message = message.trim();
message = message[0].toUpperCase() + message.slice(1);
console.log(message);
// Упражнение 3
let age = Number(prompt('Сколько вам лет?'));
let result;
switch (true) {
    case age < 0:
        alert('Вы еще не родились');
        break;
    case age >= 0 && age <= 3:
        result = "младенец";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    case age <= 11:
        result = "ребенок";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    case age <= 18:
        result = "подросток";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    case age <= 40:
        result = "познаёте жизнь";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    case age <= 80:
        result = "познали жизнь";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    case age >= 81:
        result = "долгожитель";
        alert(`Вам ${age} лет и вы ${result}`);
        break;
    default:
        alert("Быть не может!") 
}
// Упражнение 4
let message1 ='Я работаю со строками как профессионал!';
let count = message1.split(' ');
console.log(count.length);

