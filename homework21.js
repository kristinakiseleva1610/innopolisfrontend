"use strict";

// Упражнение 1
/**
 * @param {object} obj объект без свойств
 * @return {boolean} Возвращает true если у объекта нет свойств
 * @return {boolean} Возвращает false если добавить свойство
 */
let obj = {};
function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }
    return true;
}
/* obj.age = 12; */
alert(isEmpty(obj));

// Упражнение 3
/**
 * Возвращает параметр, увеличенный на 5%
 * @param {number} perzent процент
 * @param {number} salaries[key] ключ объекта (зарплата сотрудников)
 * @return {object} salaries объект с увеличенными на 5% зарплатами
 * @param {number} sum сумма увеличенных зарплат
 */

let salaries = { 
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
function raiseSalary(perzent) {
    for (let key in salaries) {
        let a = salaries[key] / 100 * perzent;
        salaries[key] = salaries[key] + a;
        Math.floor(salaries[key]);
    }
    return salaries;
}
console.log(raiseSalary(5));
let sum = 0;
for (let key in salaries) {
    sum += salaries[key];
}
console.log(sum)