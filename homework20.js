"use strict";

// Упражнение 1
let a = 0;
while (a < 21) {
    if (a % 2 === 0)
    console.log(a);
    a++;
}

// Упражнение 2
let summ = 0;

for (let i = 0; i < 3; i++) {
    let b = Number(prompt('Введите число'));
    summ += b;
    if (isNaN(b)) {
        alert('Ошибка, вы ввели не число');
        break;
    }
}
alert(summ);

// Упражнение 3
let month = Number(prompt('введите порядковый номер месяца в году'));
function getNameOfMonth(month) {
    if (month === 0) {
        return 'Январь'
    }
    if (month === 1) {
        return 'Февраль'
    }
    if (month === 2) {
        return 'Март'
    }
    if (month === 3) {
        return 'Апрель'
    }
    if (month === 4) {
        return 'Май'
    }
    if (month === 5) {
        return 'Июнь'
    }
    if (month === 6) {
        return 'Июль'
    }
    if (month === 7) {
        return 'Август'
    }
    if (month === 8) {
        return 'Сентябрь'
    }
    if (month === 9) {
        return 'Октябрь'
    }
    if (month === 10) {
        return 'Ноябрь'
    }
    if (month === 11) {
        return 'Декабрь'
    }
}
console.log(getNameOfMonth(month));

for (month = 0; month < 12 ; month++) {
    if (month === 9) 
    continue;
    console.log(getNameOfMonth(month));
}

// Упражнение 4
// Immediately Invoked Function Expression, IIFE — это функция, которая выполняется сразу же после того, как была определена.
/* ;(function () {
    // ...Тело функции
  })() */
  // Сама функция внутри:
// function () { ... }

// Это обычная функция, которая описывается и ведёт себя по всем правилам функций в JS.

// Скобки вокруг функции:
// (function() { ... })

// Скобки превращают функцию в выражение, которое можно вызвать. То есть если до этого шага мы функцию объявили, то на этом шаге мы приготовили её к мгновенному вызову.

// Скобки вызова:
// (function() { ... })()

// Последняя пара скобок вызывает выражение, то есть вызывает функцию, которую мы создали на 1-м шаге и подготовили на 2-м.

// Так как функция внутри скобок — это обычная функция, она точно так же создаёт внутри себя область видимости, доступ к которой есть только у неё. То есть всё, что внутри функции, остаётся внутри.

// При помощи IIFE мы можем использовать одинаковые названия переменных, не боясь, что они случайно перезапишут значения переменных из чужих модулей, если мы не контролируем кодовую базу полностью сами
/* (function module1() {
    const a = 42
    console.log(a)
  })()
  ;(function module2() {
    const a = '43!'
    alert(a)
  })() */
  
  


