"use strict";
let form = document.querySelector('.forms__content');
let inputName = form.querySelector('.forms__name');
let inputEstimation = form.querySelector('.forms__estimation');
let formButton = form.querySelector('.forms__btn');
let nameError = form.querySelector('.forms__name-error');
let estimationError = form.querySelector('.forms__estimation-error');

let removeErr = () => {
    inputName.classList.remove('error');
    nameError.classList.remove('error');
}

let removeError = () => {
    inputEstimation.classList.remove('error');
    estimationError.classList.remove('error');
}

form.addEventListener('submit', function (evt) {
    evt.preventDefault();
    checkName();
    checkEstimation();
    if (nameError.classList.contains('error')) {
        inputEstimation.classList.remove('error');
        estimationError.classList.remove('error');
    }
})

function checkName() {
    
    if (inputName.value.length === 0) {
        nameError.textContent = 'Вы забыли указать имя и фамилию';
        inputName.classList.add('error');
        nameError.classList.add('error');
    }
    else if (inputName.value.length < 3) {
        nameError.textContent = 'Имя не может быть короче 2-х символов';
        inputName.classList.add('error');
        nameError.classList.add('error');
    }

    inputName.addEventListener("input", removeErr)
};

function checkEstimation() {
    
    if (inputEstimation.value < 1 || inputEstimation.value > 5) {
        inputEstimation.classList.add('error');
        estimationError.classList.add('error');
        estimationError.textContent = 'Оценка должна быть от 1 до 5';
    }
    
    inputEstimation.addEventListener("input", removeError)
};