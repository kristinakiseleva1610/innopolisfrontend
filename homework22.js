"use strict";
// Упражнение 1
let arr1 = [1, 2, 10, 5]; //18
let arr2 = [1, {}, 10, "a", -5]; //6
function getSumm(arr) {
    let sum = 0;
    for (let num of arr) {
        if (typeof (num) === 'number') {
            sum += num;
        }
        else {
            continue;
        }
    }
    return sum;
};
console.log(getSumm(arr2));

// Упражнение 2
// в data.js

// Упражнение 3
// 4884, 3456
let cart = [];
function addToCart(id) {
    if(!cart.includes(id)) {
        cart.push(id);
    } 
}
addToCart(4884);
addToCart(3456);
addToCart(3456);
addToCart(3451);
addToCart(3452);
addToCart(3453);
console.log(cart);

function removeFromCart(id) {
   let index = cart.indexOf(id)
   if (index > -1) {
    cart.splice(index, 1)
   }
   return cart;
}
removeFromCart(3456);
removeFromCart(4884);
console.log(cart);

