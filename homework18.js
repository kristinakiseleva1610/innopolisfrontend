"use strict";

// Упражнение 1
let a = '100px';
let b = '323px';
let result = (parseInt(a) + parseInt(b))
console.log(result);

// Упражнение 2
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

// Упражнение 3
let d = 0.111
// Решение
console.log(Math.ceil(d))

let e = 45.333333;
// Решение
console.log(+e.toFixed(1))

let f = 3;
// Решение
console.log(f ** 5)

let g = 4e15;
// Решение
console.log(g)

let h ='1'!== 1;
// Решение
console.log(h)

// Упражнение 4
console.log(0.1+0.2===0.3); // Вернёт false, почему?
// Ответ
// В JavaScript нет возможности для хранения точных значений 0.1 или 0.2, используя двоичную систему, точно также, как нет возможности хранить одну третью в десятичной системе счисления.
// Решение
let sum = 0.1 + 0.2;
console.log(+sum.toFixed(2));
