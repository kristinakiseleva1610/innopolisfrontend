"use strict";
// Упражнение 1
let n = +prompt('Введите число');
let intervalId = setInterval (() => {
    console.log('Осталось', n);
    n = n-1;
    
    if (n === 0) {
        clearInterval(intervalId);
        console.log('Время вышло');
    }
}, 1000);

// Упражнение 2
let promise = fetch("https://reqres.in/api/users");

promise
.then(function (response) {
    return response.json();
})
.then(function (response) {
    let users = response.data;
    console.log(`Получили пользователей: ${users.length}`);
    users.forEach(function(user) {
        console.log(`- ${user.first_name} ${user.last_name} (${user.email})`)
    });
})
.catch(function() {
    console.log('Упс:(');
});
